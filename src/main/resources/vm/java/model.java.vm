package ${packageName}.${businessName}.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * ${functionName}对象 ${tableName}
 * 
 * @author ${author}
 * @since ${datetime}
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "${tableName}")
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value="${functionName}")
public class ${ClassName} {

#foreach ($column in $columns)
#if($column.isPk == 1)
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
#end
    @Column(name = "$column.javaField")
    @ApiModelProperty(value="$column.columnComment")
    private $column.javaType $column.javaField;

#end

}
