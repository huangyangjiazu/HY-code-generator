package ${packageName}.${businessName}.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ${packageName}.${businessName}.repository.${ClassName}Repository;
import ${packageName}.${businessName}.model.${ClassName};

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * ${functionName}Service业务层处理
 *
 * @author ${author}
 * @since ${datetime}
 */
@Slf4j
@Service
public class ${ClassName}Service {

    @Autowired
    private ${ClassName}Repository ${className}Repository;

    /**
     * 根据主键查询${functionName}
     *
     * @author ${author}
     * @since ${datetime}
     */
    public ${ClassName} select${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField}){
        return ${className}Repository.findById(${pkColumn.javaField}).orElse(null);
    }

    /**
     * 查询${functionName}列表
     *
     * @author ${author}
     * @since ${datetime}
     */
    public List<${ClassName}> select${ClassName}List(${ClassName} ${className}){

        Specification<${ClassName}> specification = (root, query, criteriaBuilder) -> {
            List<Predicate> predicateList = new ArrayList<>();

            predicateList.add(criteriaBuilder.equal(root.get("validFlag"),ValidFlag.FLAG0.getKey()));

//            if(StringUtils.isNotBlank(requestBody.getTaskName())){
//                predicateList.add(criteriaBuilder.like(root.get("模糊匹配字段"),"%"+"查询参数值"+"%"));
//            }
//
//            if(StringUtils.isNotBlank(requestBody.getTaskType())){
//                predicateList.add(criteriaBuilder.equal(root.get("全量匹配字段"),"查询参数值"));
//            }
//            if (StringUtils.isNotEmpty(requestBody.getCreatedDate())) {
//                Expression<String> convertedField = criteriaBuilder.function("to_char", String.class, root.get("日期格式化字段"), criteriaBuilder.literal("yyyyMMdd"));
//                predicateList.add(criteriaBuilder.equal(convertedField,"查询参数值"));
//            }

            Predicate[] p = new Predicate[predicateList.size()];
            query.where(criteriaBuilder.and(predicateList.toArray(p)));
            query.orderBy(criteriaBuilder.desc(root.get("updatedDate")));

            return query.getRestriction();
        };

        return ${className}Repository.findAll(specification);
    }

    /**
     * 新增${functionName}
     *
     * @author ${author}
     * @since ${datetime}
     */
    public ${ClassName} insert${ClassName}(${ClassName} ${className}){

        String userName = TokenUtil.getUserName();
        ${className}.setValidFlag(ValidFlag.FLAG0.getKey());
        ${className}.setCreatedDate(new Date());
        ${className}.setCreatedUser(userName);
        ${className}.setUpdatedDate(new Date());
        ${className}.setUpdatedUser(userName);
        return ${className}Repository.save(${className});
    }

    /**
     * 修改${functionName}
     *
     * @author ${author}
     * @since ${datetime}
     */
    public ${ClassName} update${ClassName}(${ClassName} ${className}){

        String userName = TokenUtil.getUserName();
        ${className}.setUpdatedDate(new Date());
        ${className}.setUpdatedUser(userName);
        return ${className}Repository.save(${className});
    }

    /**
     * 删除${functionName}信息
     *
     * @author ${author}
     * @since ${datetime}
     */
    public void delete${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField}){
        ${className}Repository.save(${ClassName}.builder()
                        .${pkColumn.javaField}(${pkColumn.javaField})
                        .validFlag(ValidFlag.FLAG1.getKey())
                        .updatedDate(new Date())
                        .updatedUser(TokenUtil.getUserName())
                        .build());
    }


}
